package pl.mrroman.web;

import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.util.Headers;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URL;
import java.util.Optional;
import java.util.Scanner;

import static org.junit.Assert.assertThat;

public class WebModuleTest {

    private Undertow server;

    @After
    public void tearDown() {
        Optional.ofNullable(server).ifPresent(Undertow::stop);
    }

    @Test(expected = FileNotFoundException.class)
    public void shouldStartupHttpServer() throws Exception {
        // given
        WebModule webModule = new WebModule(8081);
        server = webModule.start();

        // when
        new URL("http://localhost:8081/").openConnection().getContent();
    }

    @Test
    public void shouldHandleRouting() throws Exception {
        // given
        WebModule webModule = new WebModule(8081,
                Handlers.routing().add("GET", "/", (exch) -> {
                    exch.getResponseHeaders().add(Headers.CONTENT_TYPE, "text/plain");
                    exch.getResponseSender().send("OK");
                }));
        server = webModule.start();

        // when
        InputStream content = (InputStream) new URL("http://localhost:8081/").openConnection().getContent();

        // then
        try (Scanner scanner = new Scanner(content)) {
            assertThat(scanner.useDelimiter("\\A").next(), CoreMatchers.equalTo("OK"));
        }
    }

}