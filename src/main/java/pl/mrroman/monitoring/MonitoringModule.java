package pl.mrroman.monitoring;

import io.undertow.server.RoutingHandler;

import javax.sql.DataSource;

public class MonitoringModule {

    private final MonitoringEndpoints monitoringEndpoints;

    public MonitoringModule(final DataSource dataSource) {
        this.monitoringEndpoints = new MonitoringEndpoints(
                new HealthCheck(dataSource)
        );
    }

    public RoutingHandler getEndpoints() {
        return monitoringEndpoints.getEndpoints();
    }
}
