package pl.mrroman.monitoring;

import io.undertow.Handlers;
import io.undertow.server.RoutingHandler;
import io.undertow.util.Methods;

import static pl.mrroman.web.Endpoints.endpoint;

class MonitoringEndpoints {

    private final HealthCheck healthCheck;

    MonitoringEndpoints(HealthCheck healthCheck) {
        this.healthCheck = healthCheck;
    }

    RoutingHandler getEndpoints() {
        return Handlers
                .routing()
                .add(Methods.GET, "/api/monitoring/health", endpoint(healthCheck::handleRequest));
    }
}
