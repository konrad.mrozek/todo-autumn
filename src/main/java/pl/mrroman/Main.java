package pl.mrroman;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.mrroman.monitoring.MonitoringModule;
import pl.mrroman.todo.TodoModule;
import pl.mrroman.web.WebModule;

import javax.sql.DataSource;

public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    // "beans" context

    private final DataSource dataSource = createDataSource();

    private final MonitoringModule monitoringModule = new MonitoringModule(dataSource);
    private final TodoModule todoModule = new TodoModule(dataSource);

    private final WebModule webModule = new WebModule(8080,
            monitoringModule.getEndpoints(),
            todoModule.getEndpoints());

    // configuration

    private DataSource createDataSource() {
        final HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl("jdbc:h2:mem:");
        hikariConfig.setUsername("sa");
        hikariConfig.setPassword("");
        return new HikariDataSource(hikariConfig);
    }

    private void start() {
        webModule.start();
    }

    // bootstrap

    public static void main(String[] args) {
        long millis = System.currentTimeMillis();
        LOGGER.info("Staring todo-autumn...");

        new Main().start();
        LOGGER.info("Started in {} seconds", (System.currentTimeMillis() - millis) / 1000.0);
    }

}
