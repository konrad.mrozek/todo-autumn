package pl.mrroman.web;

import io.undertow.util.HeaderMap;
import io.undertow.util.HttpString;

import java.util.Collections;
import java.util.Deque;
import java.util.Map;

public class Request<T> {
    private final String relativePath;
    private final T content;
    private final HttpString requestMethod;
    private final Map<String, Deque<String>> queryParameters;
    private final HeaderMap requestHeaders;
    private final Map<String, String> pathParameters;

    public static Request<String> empty() {
        return new Request<>(
                "",
                HttpString.EMPTY,
                Collections.emptyMap(),
                new HeaderMap(),
                Collections.emptyMap(),
                "");
    }

    public Request(String relativePath,
                   HttpString requestMethod,
                   Map<String, Deque<String>> queryParameters,
                   HeaderMap requestHeaders,
                   Map<String, String> pathParameters,
                   T content) {
        this.relativePath = relativePath;
        this.requestMethod = requestMethod;
        this.queryParameters = queryParameters;
        this.requestHeaders = requestHeaders;
        this.pathParameters = pathParameters;
        this.content = content;
    }

    public String getRelativePath() {
        return relativePath;
    }

    public HttpString getRequestMethod() {
        return requestMethod;
    }

    public Map<String, Deque<String>> getQueryParameters() {
        return queryParameters;
    }

    public HeaderMap getRequestHeaders() {
        return requestHeaders;
    }

    public Map<String, String> getPathParameters() {
        return pathParameters;
    }

    public T getContent() {
        return content;
    }
}
