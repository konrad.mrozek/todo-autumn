package pl.mrroman.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;
import io.undertow.util.PathTemplateMatch;

import java.io.IOException;
import java.util.function.Function;

public class Endpoints {

    public static HttpHandler endpoint(Function<Request<String>, Response<String>> endpoint) {
        return bindableEndpoint(endpoint, Function.identity(), Function.identity());
    }

    public static <In, Out> HttpHandler jsonEndpoint(Function<Request<In>, Response<Out>> endpoint, Class<In> inClass) {
        ObjectMapper objectMapper = new ObjectMapper();

        return bindableEndpoint(endpoint,
                message -> {
                    try {
                        return objectMapper.readValue(message, inClass);
                    } catch (IOException ex) {
                        throw new IllegalArgumentException(ex);
                    }
                },
                obj -> {
                    try {
                        return objectMapper.writeValueAsString(obj);
                    } catch (IOException ex) {
                        throw new RuntimeException(ex);
                    }
                });
    }

    public static <In, Out> HttpHandler bindableEndpoint(Function<Request<In>, Response<Out>> endpoint,
                                                         Function<String, In> inBinder,
                                                         Function<Out, String> outBinder) {
        return (HttpServerExchange exchange) -> {
            PathTemplateMatch attachment = exchange.getAttachment(PathTemplateMatch.ATTACHMENT_KEY);

            if (exchange.getRequestMethod().equalToString("GET")) {
                Request<In> request = new Request<>(
                        exchange.getRelativePath(),
                        exchange.getRequestMethod(),
                        exchange.getQueryParameters(),
                        exchange.getRequestHeaders(),
                        attachment.getParameters(),
                        null);

                final Response<Out> response = endpoint.apply(request);
                exchange.setStatusCode(response.getStatusCode());
                exchange.getResponseHeaders().add(Headers.CONTENT_TYPE, response.getContentType());
                exchange.getResponseSender().send(outBinder.apply(response.getContent()));
            } else {
                exchange.getRequestReceiver().receiveFullString((finalExchange, message) -> {
                    Request<In> request = new Request<>(
                            exchange.getRelativePath(),
                            exchange.getRequestMethod(),
                            exchange.getQueryParameters(),
                            exchange.getRequestHeaders(),
                            attachment.getParameters(),
                            inBinder.apply(message));

                    final Response<Out> response = endpoint.apply(request);
                    exchange.setStatusCode(response.getStatusCode());
                    exchange.getResponseHeaders().add(Headers.CONTENT_TYPE, response.getContentType());
                    exchange.getResponseSender().send(outBinder.apply(response.getContent()));
                });
            }
        };
    }


}
