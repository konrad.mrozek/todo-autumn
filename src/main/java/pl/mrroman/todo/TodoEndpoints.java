package pl.mrroman.todo;

import io.undertow.Handlers;
import io.undertow.server.RoutingHandler;
import io.undertow.util.Methods;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import pl.mrroman.web.Request;
import pl.mrroman.web.Response;

import java.util.List;

import static pl.mrroman.db.Hibernate.transactionalFunction;
import static pl.mrroman.web.Endpoints.jsonEndpoint;

class TodoEndpoints {

    private final SessionFactory sessionFactory;

    TodoEndpoints(final SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    RoutingHandler getEndpoints() {

        return Handlers.routing()
                .add(Methods.GET, "/api/todo",
                        jsonEndpoint(transactionalFunction(sessionFactory, this::listTodos), Void.class))
                .add(Methods.GET, "/api/todo/{id}",
                        jsonEndpoint(transactionalFunction(sessionFactory, this::getTodo), Void.class))
                .add(Methods.POST, "/api/todo",
                        jsonEndpoint(transactionalFunction(sessionFactory, this::addTodo), TodoItem.class))
                .add(Methods.PUT, "/api/todo/{id}",
                        jsonEndpoint(transactionalFunction(sessionFactory, this::updateTodo), TodoItem.class))
                .add(Methods.DELETE, "/api/todo/{id}",
                        jsonEndpoint(transactionalFunction(sessionFactory, this::deleteTodo), Void.class));
    }

    private Response<List> listTodos(Session session, Request<Void> inRequest) {
        return new Response<>(
                200,
                "application/json",
                session.createQuery("SELECT t FROM TodoItem t").getResultList());
    }

    private Response<TodoItem> getTodo(Session session, Request<Void> inRequest) {
        TodoItem todoItem =
                session.get(TodoItem.class, Long.parseLong(inRequest.getPathParameters().get("id")));

        if (todoItem != null) {
            return new Response<>(200, "application/json", todoItem);
        } else {
            return new Response<>(404, "application/json", null);
        }
    }

    private Response<TodoItem> addTodo(Session session, Request<TodoItem> inRequest) {
        TodoItem todoItem = (TodoItem)session.merge(inRequest.getContent());

        return new Response<>(201, "application/json", todoItem);
    }

    private Response<TodoItem> updateTodo(Session session, Request<TodoItem> inRequest) {
        TodoItem todoItem = (TodoItem)session.merge(inRequest.getContent());

        if (todoItem != null) {
            return new Response<>(200, "application/json", todoItem);
        } else {
            return new Response<>(404, "application/json", null);
        }
    }

    private Response<Void> deleteTodo(Session session, Request<Void> inRequest) {
        TodoItem todoItem =
                session.get(TodoItem.class, Long.parseLong(inRequest.getPathParameters().get("id")));
        session.delete(todoItem);

        return new Response<>(200, "application/json", null);
    }

}
