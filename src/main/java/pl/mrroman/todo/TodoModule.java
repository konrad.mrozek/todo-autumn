package pl.mrroman.todo;

import io.undertow.server.RoutingHandler;
import org.hibernate.SessionFactory;
import pl.mrroman.db.Hibernate;

import javax.sql.DataSource;

public class TodoModule {

    private final SessionFactory sessionFactory;
    private final TodoEndpoints todoEndpoints;

    public TodoModule(DataSource dataSource) {
        sessionFactory = Hibernate.initSessionFactory(dataSource, TodoItem.class);
        todoEndpoints = new TodoEndpoints(sessionFactory);
    }

    public RoutingHandler getEndpoints() {
        return todoEndpoints.getEndpoints();
    }
}
